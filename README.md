# Author: Farhana Yasmin
# Instructions
* Open visual studio code
* Update visual studdio code by download the .NET Core 2.2 SDK to update Visual Studio (2.2.x+)
* Create a new Project using ASP.NET Core and name it "MvcMusicStore"
* load the "MvcMusicStore.sql" database in MS SQL server and execute it
* Open Database in Visual Studio(View > Server Explorer)
* Genetare a model in Nuget Package Manager Console using follwing scaffolding command
 Scaffold-DbContext �Connection "Server=DESKTOP-RJD4TUJ\SQLEXPRESS;Database=Patients;Trusted_Connection=True;" -Provider "Microsoft.EntityFrameworkCore.SqlServer" -OutputDir "Models" �Context "PatientsContext" �Verbose -Force
* Register the Context with Dependency Injection
* Centralise the connection string by putting this into appsettings.json alongside the default connection string
Connection String: 
"MvcMusicStoreConnection": "Server=DESKTOP-RJD4TUJ\SQLEXPRESS;Database=MvcMusicStore;Trusted_Connection=True;",
* Register a Service to Configure Context in Startup.cs

 // **context - enable dependency injection for context of MvcMusicStore database
    services.AddDbContext<MvcMusicStoreContext>(options =>
        options.UseSqlServer(Configuration.GetConnectionString("MvcMusicStoreConnection")));

# Licence
Please read `LICENSE.md` file carefully for licensing information. 
This application uses GNU open source licensing used from https://www.gnu.org/licenses/gpl-3.0.en.html

# Rational behind using open source
The application used open source licensing so that other students can use it as an example for their very first MVC Web application.


